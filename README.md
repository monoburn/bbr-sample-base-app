# Sample Base app for Bouvet Battle Royal

## Intro
A very simple solution using serveral terminal windows and single static html page.

See screenshot **bbr-base-terminals.png**.

Http requests to the central game server is issued from the various terminals. One terminal for each type of request. Press 'up/down-arrow' to quickly reuse a previous command.

The response from the server can then be copied over to a text editor for further massaging, or can be piped to another program for rapid processing.

A static html page includes a Google map centered on Oscarsborg.

Updating the map is done through entering function alls in the console tab of Chrome Dev tools.

**Curl commands to issue requests to the API:**
```
#!bash

hentpifposisjon: curl -sS http://bouvet-code-camp.azurewebsites.net/api/game/base/hentpifposisjon/ABC
hentgjeldendepost: curl -sS http://bouvet-code-camp.azurewebsites.net/api/game/base/hentgjeldendepost/ABC
hentregistrertekoder: curl -sS http://bouvet-code-camp.azurewebsites.net/api/game/base/hentregistrertekoder/ABC
sendpifmelding: curl -sS -X POST -H 'Content-Type: application/json' -d '{"Type": "Fritekst", "Innhold": "base 1 kaller pif", "LagId": "ABC"}'      http://bouvet-code-camp.azurewebsites.net/api/game/base/sendpifmelding/

```

You can also use the Chrome plugin Postman to send requests, import the attached file **bbr-api-public.json**

## Adding markers to the map
A specific coordinate can be marked in the map by attaching a Marker object to the Map object.
Use the javascript method addMarker or addMarkerSimple. A marker of type 'library' shows a person; nice to indicate where your guy is. The type 'parking' shows a 'P', which is natural to indicate the location of the 'post'.
Example: addMarkerSimple(59.676, 10.6063, "info")

## And one more thing
To automatically configure all the terminals I use [tmux](http://tmux.sourceforge.net/) and [tmuxinator](https://github.com/tmuxinator/tmuxinator). There is a yml available in this project. You can configure your terminals manually, of course.

I use a tool called 'jq' to parse the json from the http server. You can do it manually, of course.

Use of 'jq':

```
#!bash
curl -sS "http://bouvet-code-camp.azurewebsites.net/api/game/..." | jq --raw-output '"addMarkerSimple(\(.latitude),\(.longitude),\"info\")"'

```

which outputs addMarkerSimple(59.6765,10.605946,"info"). Paste that into the html map file.

## Resources
* Google Maps API documentation: https://developers.google.com/maps/documentation/javascript/basics
* Use of curl: http://httpkit.com/resources/HTTP-from-the-Command-Line/
* jq: http://stedolan.github.io/jq/ As I'm on a Mac, I installed it using 'brew install jq'
* Om posisjon på [wiki](http://en.wikipedia.org/wiki/Decimal_degrees)